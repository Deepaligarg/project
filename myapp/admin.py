from django.contrib import admin
from myapp.models import customer,category,product,cart,order,mail

# Register your models here.

admin.site.register(customer)
admin.site.register(category)

admin.site.register(product)
admin.site.register(cart)
admin.site.register(order)

admin.site.register(mail)


