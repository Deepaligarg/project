from django.urls import path
from myapp import views 
app_name = 'myapp'

urlpatterns = [
    path('front/',views.front,name = 'fr'),
    path('about/',views.about,name = 'ab'),
    path('mail/',views.usermail,name = 'ma'),
    
    path('mobiles/',views.mobiles,name = 'mobile'),
    path('registration/',views.registration,name = 'registration'),
    path('uslogin/',views.uslogin,name = 'uslogin'),
    path('uslogout/',views.uslogout,name = 'uslogout'),
    path('dashboard/',views.dashboard,name = 'dashboard'),
    path('profile/',views.profile,name = 'profile'),
    path('changeprofile/',views.change_profile,name = 'change_profile'),
    path('changepass/',views.change_password,name = 'change_password'),
    path('addcart/',views.addcart,name = 'cart'),
    path('getproduct/',views.getproduct,name = 'getproduct'),
    path('singleproduct/',views.singleproduct,name = 'singleproduct'),
    path('orders/',views.my_orders,name = 'myorders'),

    path('removecart/',views.removeCart,name='removecart'),
    path('grandTotal/',views.grandTotal,name='grandTotal'),
    path('sendMail/',views.sendMail,name='sendMail'),
    path('get_order/',views.get_order, name='get_order'),
    path('success_payment/',views.success_payment, name='success_payment'),
    path('order_details/',views.order_details, name='order_details'),
    path('pending_orders/',views.pending_orders, name='pending_orders'),
    path('uservalid/',views.uservalid,name='uservalid'),
    

    

    
    








    



   

]
